using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models;

public class Product
{
    public int Id { get; set; }
    public double Value { get; set; }
    public string Status { get; set; } = "Aguardando pagamento";
}