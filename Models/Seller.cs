using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models;

public class Seller
{
    public int Id { get; set; }
    public string SellerName { get; set; }
}