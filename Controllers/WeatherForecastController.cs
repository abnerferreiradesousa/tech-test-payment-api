using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers;

[ApiController]
[Route("[controller]")]
public class WeatherForecastController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private static Sale[] Sales = new[] 
    {
        new Sale 
        { 
            Id = 1,
            Seller = new Seller 
            {
                Id = 1,
                SellerName = "Augusto",
            },
            ProductsSold = new List<Product> 
            { 
                new Product
                {
                    Id = 1,
                    Status = "sjskjdkljadj",
                    Value = 50.1,
                }
            }  
        },
    };

    private readonly ILogger<WeatherForecastController> _logger;

    public WeatherForecastController(ILogger<WeatherForecastController> logger)
    {
        _logger = logger;
    }

    [HttpGet("{id}")]
    public ActionResult Get(int id)
    {
        var sale = Sales.First(x => x.Id == id);
        if(sale == null) 
        {
            return NotFound();
        }
        return Ok(sale);
    }

    [HttpGet()]
    public List<Sale> Get()
    {
        return Sales.ToList();
    }

    [HttpPost()]
    public IEnumerable<Sale> Register(Sale saleRequest)
    {
        Sale sale = saleRequest;
        sale.Seller.Id = Sales.Length + 1;

       
        return Sales.Append(sale);
    } 
}
